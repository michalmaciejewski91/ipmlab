﻿using IPMLab.service;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace IPMLab
{
    public class CurrencyViewModel : INotifyPropertyChanged
    {
        Windows.Storage.ApplicationDataContainer localSettings;
        Windows.Storage.ApplicationDataCompositeValue composite;

        private NBPConnectionService connectionService;

        private DateTimeOffset? calendarDate;
        public DateTimeOffset? CalendarDate
        {
            get { return this.calendarDate; }
            set
            {
                this.calendarDate = value;
                StoreLocalSettings();
                LoadCurrenciesAsync();
            }
        }

        private ObservableCollection<Rate> currencies;
        private ObservableCollection<Rate> filteredCurrencies;
        public ObservableCollection<Rate> FilteredCurrencies
        {
            get { return this.filteredCurrencies; }
            set
            {
                this.filteredCurrencies = value;
                this.OnPropertyChanged();
            }
        }

        public CurrencyViewModel()
        {
            connectionService = new NBPConnectionService();
            localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
            composite = (Windows.Storage.ApplicationDataCompositeValue)localSettings.Values["CurrencyViewModel"];
            if (composite == null)
            {
                composite = new Windows.Storage.ApplicationDataCompositeValue();
                InitData();
                StoreLocalSettings();
            }
            else
            {
                CollectDataFromLocalSettings();
            }
            LoadCurrenciesAsync();
        }

        private async void LoadCurrenciesAsync()
        {
            currencies = await connectionService.GetCurrenciesFromNBPAsync(calendarDate);        
            FilteredCurrencies = new ObservableCollection<Rate>(currencies);
        }

        private void InitData()
        {
            calendarDate = DateTimeOffset.Now;
        }

        private void CollectDataFromLocalSettings()
        {
            this.calendarDate = (DateTimeOffset?)composite["calendarDate"];
        }

        public void StoreLocalSettings()
        {
            composite["calendarDate"] = calendarDate.Value;
            localSettings.Values["CurrencyViewModel"] = composite;
        }

        public void Filter(String filterCurrencyCode)
        {
            ObservableCollection<Rate> temp = new ObservableCollection<Rate>(currencies);
            if (filterCurrencyCode != null && filterCurrencyCode.Length > 0)
            {
                foreach (Rate currency in currencies)
                {
                    if (!currency.Code.Contains(filterCurrencyCode))
                    {
                        temp.Remove(currency);
                    }
                }
            }
            FilteredCurrencies = new ObservableCollection<Rate>(temp);
        }

        public event PropertyChangedEventHandler PropertyChanged = delegate { };
        public void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            // Raise the PropertyChanged event, passing the name of the property whose value has changed.
            this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
