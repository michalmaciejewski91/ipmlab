﻿using IPMLab.model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace IPMLab.service
{
    public class NBPConnectionService
    {
        private HttpClient httpClient;
        private static string ADDRESS;
        private static string FORMAT;

        public NBPConnectionService()
        {
            httpClient = new HttpClient();
            ADDRESS = "http://api.nbp.pl/api/exchangerates/tables/a/";
            FORMAT = "?format=json";
        }

        public async Task<ObservableCollection<Rate>> GetCurrenciesFromNBPAsync(DateTimeOffset? date)
        {
            try
            {
                string stringDate = date != null ? ((DateTimeOffset)date).ToString("yyyy-MM-dd", CultureInfo.CurrentCulture) : "N/A";
                string fullAddress = ADDRESS + stringDate + FORMAT;
                HttpResponseMessage responseGet = await httpClient.GetAsync(fullAddress);
                String json = await responseGet.Content.ReadAsStringAsync();
                ExchangeRatesTable[] table = JsonConvert.DeserializeObject<ExchangeRatesTable[]>(json);
                return table[0].Rates;
            } catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return new ObservableCollection<Rate>();
            }

        }
    }
}
