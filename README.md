# IPMLab
Interfejsy Platform Mobilnych - Project created for university purpose


## dr inż. Radosław Wajman

## Wytyczne projektu - Windows C# Application Store

**Cel ćwiczenia:**
W trakcie realizacji zadania student opracuje aplikację w języku C# typu Windows Universal Platform w Microsoft Visual
Studio 2017, której zadaniem będzie pobieranie i odpowiednie zarządzanie oraz wyświetlanie danych kursów średnich walut
obcych z serwisu NBP w ramach jednego z możliwych sposobów:

- [http://www.nbp.pl/home.aspx?f=/kursy/instrukcja_pobierania_kursow_walut.html](http://www.nbp.pl/home.aspx?f=/kursy/instrukcja_pobierania_kursow_walut.html).
- [http://api.nbp.pl/](http://api.nbp.pl/)

**Polecenie ćwiczeniowe:**
Do zaliczenia zadania wymagane jest napisanie programu, który posiada następujące cechy:

1. Dwie strony (Page). Na głównej stronie „ _Średnie kursy walut_ ” należy zaprojektować następujące panele:
    a. panel wyświetlający w czytelnie sformatowanej tabelce nazwę waluty oraz jej kurs na bieżący dzień; Po
       kliknięciu na wybraną walutę z tabelki sterowanie programu powinno zostać przekazane do następnej strony
       zatytułowanej „ _Historia kursu ???_ ”, gdzie ??? oznacza symbol wybranej waluty – szczegóły patrz punkt 3
       niniejszej specyfikacji;
    b. panel (np. obok po lewej) wyświetlający listę dat opublikowanych plików z innych dni; Po wybraniu dowolnej
       pozycji z w/w listy zawartość panelu z punktu (a) powinna zostać zmodyfikowana o dane odczytane z tabeli z
       NBP dla wybranej daty;
    c. pasek aplikacji na górze zawierający przynajmniej ikonę do wyjścia z programu; Mile widziane inne opcje;
2. Na drugiej stronie „ _Historia waluty ???”_ należy zaprojektować następujące elementy interfejsu użytkownika oraz ich
    funkcjonalności:
       a. kontrolki umożliwiające szybkie i łatwe ustalenie daty początkowej oraz końcowej wyświetlania historii kursu
          danej waluty (uwaga: należy zabronić ustawienia daty starszej niż tej, dla której opublikowano dane
          dot. walut w serwisie NBP);
       b. element graficzny rysujący wykres przebiegu kursu danej waluty (w tym np. etykiety osi, legenda, siatka itp.)
          w przedziale czasowym określonym przez w/w kontrolki;
       c. pasek postępu wczytywania nowych danych z NBP oraz zapisywanie lokalnie już raz pobranych danych,
       d. możliwość zapisania wykresu do pliku np. JPG z opcją wskazania katalogu docelowego (zaimplementowane
          np. w postaci dolnego paska aplikacji i odpowiedniego przycisku);
       e. mile widziane jest zaimplementowanie gestu powrotu do poprzedniej strony poprzez przesunięcie
          palcem/myszką w kierunku prawym;
       f. pasek aplikacji zawierający przynajmniej przyciski powrotu do poprzedniej strony oraz wyjścia z aplikacji;
3. Aplikacja powinna mieć zaimplementowany mechanizm przywracania stanu aplikacji i nawigacji obiektów Page
    oraz przechowywać dane użytkownika tj.:
       a. ostatnio otwartą stronę aplikacji;
       b. w przypadku strony pierwszej - datę, dla której ostatnio były wyświetlane kursy walut w tabelce (punkt 2a)
          (chyba, że data nie istnieje już w archiwum NBP, i wtedy wyświetlić tabelę dla daty starszej);
       c. dla strony historii waluty ostatnio przeglądaną walutę wraz z ustawionymi datami początkową i końcową;
4. Do poprawnego wykonania zadania na pewno przyda się: 
    
    a. klasa HTTPClient i inne klasy, które posłużą do pobrania dokumentu XML i TXT ze strony NBP;
    ```
    Przydatne opracowania:
    ```
    - [http://www.asp.net/web-api/overview/advanced/calling-a-web-api-from-a-net-client](http://www.asp.net/web-api/overview/advanced/calling-a-web-api-from-a-net-client)
    - [http://www.dreamincode.net/forums/topic/370811-wait-for-task-to-complete/](http://www.dreamincode.net/forums/topic/370811-wait-for-task-to-complete/)
    - Pobieranie jednego dokumenty (NBP API) i raportowanie progresu lub tutaj
    - https://msdn.microsoft.com/en-us/library/jj155756.aspx

    b. przestrzeń nazw Windows.UI.Input.Inking pomocna w rysowaniu
    ```
    http://code.msdn.microsoft.com/windowsapps/Windows-8-Input-Ink-sample-
    http://code.msdn.microsoft.com/InkPen-sample-in-CSharp-189ce853#content
    http://code.msdn.microsoft.com/windowsapps/Input-simplified-ink-sample-11614bbf#content
    do rysowania można również użyć biblioteki WinRT XAML Toolkit (przykład użycia) lub SyncFusion
    ```

    c. parser plików XML
    ```
    http://www.geekchamp.com/articles/how-to-read-xml-files-in-windows-8---winrt
    http://stackoverflow.com/questions/566167/query-an-xdocument-for-elements-by-name-at-any-depth
    http://stackoverflow.com/questions/11496570/hi-how-to-parse-xml-with-descendants-within-descendants-c-sharp
    http://stackoverflow.com/questions/14297947/xml-parsing-in-winrt
    https://chodounsky.net/2013/10/23/descendants-vs-elements-in-linq-to-xml/
    ```

    d. można wykorzystać inne źródła – wedle uznania.



**Kryteria oceny:**
Ocenie podlegać będą:

1. Zgodność aplikacji z wytycznymi,
2. W pełni wykorzystanie mechanizmów Data Binding, Windows.Storage, app lifecycle and state,
3. Przyjazny i intuicyjny interfejs użytkownika wolny od błędów niepoprawnego użytkowania,
4. Awaryjność, Czytelność kodu,
5. Na ocenę 3 wystarczy prawidłowa implementacja funkcjonalności tylko pierwszej strony „ _Średnie kursy walut_ ”.


